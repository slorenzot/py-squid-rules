#!/usr/bin/env python
#
#
#

import sys
import os
import argparse
import re

requireRoot=False

rules = []



#
# retorna una lista de etiquetas especificas
#
def parse_tag(config):
	return re.match(r'\#\s(@[a-z]{3,})\s+(\S+)', config)

# 
# Valida la direccion MAC suministrada
# 
def parse_mac(config):
	return re.match(r'(?:[A-Fa-f0-9]{2}[:-]){5}(?:[A-Fa-f0-9]{2})', config)

#
# retorna una con el texto de configuracion del host
#
def parse_host(config):
	return re.findall(r'(?:\###\s+macaddress\s+)(?:\#\s@[a-z]{3,}\s+(?:\S+)\s+){1,6}((?:[A-Fa-f0-9]{2}[:-]){5}(?:[A-Fa-f0-9]{2}))', config)


def read_rules_file(to_stdout):
	global rules
	tag = []
	mac = []

	if not os.path.exists("squid.rules"):
		print "** ERROR: File rules not exist!"
		sys.exit(1)

	print "Reading rules file..."
	file = open("squid.rules", "r").read()

	found_macs = parse_host(file)

	for line in parse_host(file):
		print line
	# 	if (parse_tag(line)):
	# 		print parse_tag(line)

	# 	if (parse_mac(line)):
	# 		mac.append(line)

	print "%d rules existing..." % len(found_macs)

	if (to_stdout):
		for rul in rules:
			print "%s" % rul


def do_add(mac):
	rules = read_rules_file(False)

	if not parse_mac(mac) == None:
		print "add %s MAC" % mac
	else: print("** ERROR: MAC address %s is invalid! bye" % mac)


def do_rem(mac):
	rules = read_rules_file(False)

	if not parse_mac(mac) == None:
		print "rem %s MAC" % mac
	else: print("** ERROR: MAC address %s is invalid! bye" % mac)

def do_list():
	read_rules_file(True)
	parser.print_usage()


def info():
	print "Command info!"


def version():
	print "Command version!"



if __name__=="__main__":
	if requireRoot and not os.geteuid() == 0:
		sys.exit("\n** Only root can run this script, bye!\n")

	parser = argparse.ArgumentParser()
	parser.add_argument('--list','-l', 
		action='store_true',
		help='Show all MAC address registered & privilegies')
	parser.add_argument('--add','-a', 
		help='Add privilegies into SQUID a MAC address')
	parser.add_argument('--remove','-r', 
		help='Add privilegies into SQUID a MAC address')
	parser.add_argument('--version','-v', 
		action='version', 
		help='show command version')
	parser.add_argument('--verbose', 
		action='store_true', 
		help='increase output verbosity')

	args = parser.parse_args()

	try:
		if (args.list):
			do_list()

		if (args.add):
			do_add(args.add)

		if (args.remove):
			do_remove(args.remove)
			
	except KeyError:
		parser.print_usage()

	parser.exit()